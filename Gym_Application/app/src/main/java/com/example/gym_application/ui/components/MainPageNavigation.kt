package com.example.gym_application.ui.components

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.Icons.Filled
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.List
import androidx.compose.material.icons.outlined.Home
import androidx.compose.material3.Icon
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Text
import com.example.gym_application.ui.navigation.Screen

import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.navigation.NavController
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.compose.currentBackStackEntryAsState
import com.example.gym_application.ui.navigation.screens

//The Main Page Navigation Bar is used to navigate between HomeScreen and ListOfAllExercises
@Composable
fun MainPageNavigationBar(navController: NavController){
    //Sets the icons for the navigation bar
    val icons = mapOf(
        Screen.Home to IconGroup(
            filledIcon = Icons.Filled.Home,
            outlineIcon = Icons.Outlined.Home,
            label = "HOME"
        ),
        Screen.ListOfAllExercises to IconGroup(
            filledIcon = Icons.Filled.List,
            outlineIcon = Icons.Filled.List,
            label = "LIST"
        )
    )
    NavigationBar {
        val navBackStackEntry by navController.currentBackStackEntryAsState()
        val currentDestination = navBackStackEntry?.destination
        //For the list of screens, which contains the new screens on the nav bar
        for (index in 0 until minOf(screens.size, 2)) {
            //If the screen is selected then make sure it is highlighted in the nav bar and navigate to the screen
            val screen = screens[index]
            val isSelected = currentDestination?.hierarchy?.any { it.route == screen.route } == true
            val labelText = icons[screen]!!.label
            NavigationBarItem(
                selected = isSelected,
                onClick = {
                    navController.navigate(screen.route) {
                        popUpTo(navController.graph.findStartDestination().id) {
                            saveState = true
                        }
                        // Avoid multiple copies of the same destination when
                        // reselecting the same item
                        launchSingleTop = true
                        // Restore state when reselecting a previously selected item
                        restoreState = true
                    } },
                icon = { Icon(
                    imageVector = (if (isSelected)
                        icons[screen]!!.filledIcon
                    else
                        icons[screen]!!.outlineIcon),
                    contentDescription = labelText) },
                label = {Text(labelText)}
            )
        }
    }
}