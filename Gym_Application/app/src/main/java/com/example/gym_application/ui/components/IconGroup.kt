package com.example.gym_application.ui.components

import androidx.compose.ui.graphics.vector.ImageVector

data class IconGroup(
    val filledIcon: ImageVector,
    val outlineIcon: ImageVector,
    val label: String
)
