package com.example.gym_application.ui.model

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.gym_application.ui.datasource.ExerciseRepository

//Exercise viewmodel -> for manipulating live data within the app
class ExerciseViewModel(application: Application) : AndroidViewModel(application){
    private val repository:ExerciseRepository = ExerciseRepository(application)
    var allExercises: LiveData<List<Exercise>> = loadAllExercises()

    private fun loadAllExercises():LiveData<List<Exercise>>{
        return repository.getAllExercises()
    }

    internal fun updateExercise(exercise: Exercise){

        repository.updateExercise(exercise)
    }

    internal fun addExercise(exercise: Exercise){
        repository.insert(exercise)
    }

    internal fun deleteExercise(exercise: Exercise){
        repository.remove(exercise)
    }


}