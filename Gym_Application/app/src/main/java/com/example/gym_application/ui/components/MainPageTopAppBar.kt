package com.example.gym_application.ui.components

import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text

@OptIn(ExperimentalMaterial3Api::class)
@Composable
//Main Page Top App Bar is used give a title to each of the screen.
//Main screens can be navigated with the MainPageNav
//Non main screens have a back button
fun MainPageTopAppBar(string: String,    onClick: () -> Unit = {})
{
    CenterAlignedTopAppBar(
        title = {Text(string)},
    )
}