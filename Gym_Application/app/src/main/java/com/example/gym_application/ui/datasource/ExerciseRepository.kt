package com.example.gym_application.ui.datasource

import android.app.Application
import androidx.lifecycle.LiveData
import com.example.gym_application.ui.model.Exercise
import com.example.gym_application.ui.model.Session

//Repository For Accesing and using the ExerciseDao
class ExerciseRepository(application: Application) {
    private val exerciseDao = ExerciseDataBase.getDatabase(application)?.exerciseDao()?: throw IllegalStateException("ExerciseDao cannot be null. Database initialization failed.")

    //Add an exercise
    fun insert(exercise: Exercise){
        exerciseDao.insertSingleExercise(exercise)
    }

    //Add multiple exercises
    fun insertMultipleExercise(exerciseList: List<Exercise>){
        exerciseDao.insertMultipleExercise(exerciseList)
    }

    //get a list of all the exercises
    fun getAllExercises(): LiveData<List<Exercise>> = exerciseDao.getAllExercises()

    //update an existing exercise
    fun updateExercise(exercise: Exercise){
        exerciseDao.updateExercise(exercise)
    }

    //remove an exisiting exercise
    fun remove(exercise: Exercise){
        exerciseDao.deleteExercise(exercise)
    }


}