package com.example.gym_application.ui.model

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface ExerciseDao {
    @Insert
    fun insertSingleExercise(exercise: Exercise)

    @Insert
    fun insertMultipleExercise(exerciseList: List<Exercise>)

    @Update
    fun updateExercise(exercise: Exercise)

    @Delete
    fun deleteExercise(exercise: Exercise)

    @Query("DELETE FROM exercises")
    fun deleteAll()

    @Query ("SELECT * FROM exercises")
    fun getAllExercises(): LiveData<List<Exercise>>

}