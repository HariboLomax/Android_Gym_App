package com.example.gym_application.ui.model

import java.math.BigDecimal
import java.math.RoundingMode

//Function to convert x seconds into "x h x m x s" format
fun SecondsToHourFormat(seconds : Int) : String{
    val hours = seconds / 3600
    val minutes = (seconds % 3600) / 60
    val seconds = seconds % 60
    return "$hours h $minutes m $seconds s"
}