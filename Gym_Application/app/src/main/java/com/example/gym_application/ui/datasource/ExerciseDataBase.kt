package com.example.gym_application.ui.datasource

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.gym_application.R

import com.example.gym_application.ui.model.Exercise
import com.example.gym_application.ui.model.ExerciseDao
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


@Database(entities = [Exercise::class],version = 1)
//DataBase For Storing Exercises
abstract class ExerciseDataBase : RoomDatabase() {
    abstract fun exerciseDao(): ExerciseDao

    companion object {
        private var instance: ExerciseDataBase? = null
        private val coroutineScope = CoroutineScope(Dispatchers.IO)


        @Synchronized
        fun getDatabase(context: Context): ExerciseDataBase {
            return synchronized(this) {
                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        ExerciseDataBase::class.java,
                        "exercise_database"
                    )
                        .allowMainThreadQueries()
                        .addCallback(databaseCallback(context))
                        //.addMigrations(MIGRATION_1_2, MIGRATION_2_3)
                        .build()
                }
                instance!!
            }
        }
        private fun databaseCallback(context: Context): Callback {
            return object : Callback() {
                override fun onCreate(db: SupportSQLiteDatabase) {
                    super.onCreate(db)
                    coroutineScope.launch {
                        populateDatabase(context, getDatabase(context)!!)
                    }
                }
            }
        }

        private fun populateDatabase(context: Context,instance : ExerciseDataBase){
            val shoulderPress =
                Exercise(
                    0,
                    "Shoulder Press",
                    "Deltoid, Triceps, Trapezius",
                    "file:///android_asset/images/shoulderpress.jpg",
                    15.0,
                    0.0,
                    0.0,
                    8,
                    3,
                    false,
                    60,
                    "Sit in the seat, sitting upright, feet firmly planted on the ground.\n" +
                            "Grasp the handles firmly, wrapping your thumb around the handle.\nSlowly push up until fully extended.\n" +
                            "Bring back down until your elbows are inline with your shoulders, repeat for x reps"
                )
            val benchPress =
                Exercise(
                    0,
                    "Bench Press",
                    "Chest, Front Delts, Triceps",
                    "file:///android_asset/images/benchpress.jpg",
                    55.0,
                    40.0,
                    30.0,
                    6,
                    3,
                    true,
                    40,
                    "Lie on the bench with your eyes under the bar\n" +
                            "Grab the bar with a medium grip-width (thumbs around the bar!)\n" +
                            "Unrack the bar by straightening your arms\n" +
                            "Lower the bar to your mid-chest\n" +
                            "Press the bar back up until your arms are straight"
                )
            val exerciseList = mutableListOf(
                shoulderPress,
                benchPress
            )
            val dao = instance.exerciseDao()
            dao.insertMultipleExercise(exerciseList)
        }

    }



}