package com.example.gym_application.ui.datasource

import android.app.Application
import android.util.Log
import androidx.lifecycle.LiveData
import com.example.gym_application.ui.model.Session

//Repository For Accesing and using the SessionDao
class SessionRepository(application: Application) {
    private val sessionDao = SessionDataBase.getDatabase(application)?.sessionDao()?: throw IllegalStateException("ExerciseDao cannot be null. Database initialization failed.")

    //Add a session
    fun insert(session: Session){
        sessionDao.insertSingleSession(session)
    }

    //Add multiple sessions
    fun insertMultipleSessions(sessionList: MutableList<Session>){
        sessionDao.insertMultipleSession(sessionList)
    }

    //get all sessions
    fun getAllSessions(): LiveData<List<Session>> = sessionDao.getAllSession()

    //update an exising session
    fun updateSession(session: Session){
        sessionDao.updateSession(session)

    }

    //remove a session
    fun remove(session: Session){
        sessionDao.deleteSession(session)
    }

}