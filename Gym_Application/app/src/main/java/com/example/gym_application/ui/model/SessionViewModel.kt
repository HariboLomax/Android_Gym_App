package com.example.gym_application.ui.model

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.gym_application.ui.datasource.ExerciseRepository
import com.example.gym_application.ui.datasource.SessionRepository
import kotlinx.coroutines.launch

//Session viewmodel -> for manipulating live data within the app
class SessionViewModel(application: Application) : AndroidViewModel(application){
    private val repository: SessionRepository = SessionRepository(application)
    var allSessions: LiveData<List<Session>> = loadAllSessions()

    private fun loadAllSessions(): LiveData<List<Session>> {
        return repository.getAllSessions()
    }

    internal fun updateSession(session: Session){
        viewModelScope.launch {
            repository.updateSession(session)
        }

    }

    internal fun addSession(session: Session){
        repository.insert(session)
    }

    internal  fun deleteSession(session: Session) {
        repository.remove(session)
    }


}