package com.example.gym_application.ui.navigation

//List of all the screens
sealed class Screen(val route: String, val arguement : String) {
    //Screens with "" as an argument do not take an argument
    object Home : Screen("home","")
    object ListOfAllExercises : Screen("listofallexercises","")
    object AddExerciseForm : Screen("addexerciseform","")
    object EditExerciseForm : Screen("addexerciseform","exercise")
    object SessionScreen : Screen("sessionscreen","day")
    object ExerciseInfoScreen : Screen("exerciseinfoscreen","exercisename")

    //Screens without an argument have a simple route path
    //Screens with an argument have their argument put at the end of the route path "/argument
    fun routePath() =
        if(arguement.isEmpty())
            route
        else
            "${route}/{${arguement}}"
    val basePath = "${route}/"
}

//List of the screens to be on the bottom app bar
val screens = listOf(
    Screen.Home,
    Screen.ListOfAllExercises,
)