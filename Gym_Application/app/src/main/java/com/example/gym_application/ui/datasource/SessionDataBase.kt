package com.example.gym_application.ui.datasource




import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.gym_application.R
import com.example.gym_application.ui.datasource.util.ExerciseList_Converter
import com.example.gym_application.ui.model.Exercise
import com.example.gym_application.ui.model.Session
import com.example.gym_application.ui.model.SessionDao
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

//DataBase For Storing Sessions
@Database(entities = [Session::class, Exercise::class], version = 1)
//Type converter to be used for converting a Exercise<List> into a string
@TypeConverters(ExerciseList_Converter::class)
abstract class SessionDataBase : RoomDatabase() {
    abstract fun sessionDao(): SessionDao

    companion object {
        private var instance: SessionDataBase? = null
        private val coroutineScope = CoroutineScope(Dispatchers.IO)

        @Synchronized
        fun getDatabase(context: Context): SessionDataBase {
            return synchronized(this) {
                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        SessionDataBase::class.java,
                        context.getString(R.string.session_database)
                    )
                        .allowMainThreadQueries()
                        .addCallback(databaseCallback(context))
                        //.addMigrations(MIGRATION_1_2, MIGRATION_2_3)
                        .build()
                }
                instance!!
            }
        }

        private fun databaseCallback(context: Context): Callback {
            return object : Callback() {
                override fun onCreate(db: SupportSQLiteDatabase) {
                    super.onCreate(db)
                    coroutineScope.launch {
                        populateDatabase(
                            context,
                            getDatabase(context)!!
                        )
                    }
                }
            }
        }

        private fun populateDatabase(context: Context, instance: SessionDataBase) {
            val newList: List<Exercise> = mutableListOf()
            val monday =
                Session(
                    0,
                    "Monday",
                    newList,
                    "[Session Name]"
                )
            val tuesday =
                Session(
                    0,
                    "Tuesday",
                    newList,
                    "[Session Name]"
                )
            val sessionList = mutableListOf(
                monday,
                tuesday
            )
            val dao = instance.sessionDao()
            dao.insertMultipleSession(sessionList)
        }
    }
}