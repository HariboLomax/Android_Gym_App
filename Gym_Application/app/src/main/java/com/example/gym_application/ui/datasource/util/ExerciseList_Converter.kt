package com.example.gym_application.ui.datasource.util

import androidx.room.TypeConverter
import com.example.gym_application.ui.model.Exercise


//ExerciseList_Converter is used to convert a List<Exercise> to a string and back again
object ExerciseList_Converter {
    @TypeConverter
    @JvmStatic
    //Converting a string to a List<Exercise>
    fun fromExerciseList(value: String): List<Exercise> {
        val exerciseList = mutableListOf<Exercise>()
        //Split each exercise up using a character that is unlikely to be used
        val items = value.split("¬")
        //For every variable in the exercise
        for (item in items) {
            //Split each exercise value up using a character that is unlikey to be used
            //This has a major weakness that if this character is typed in the information or name tab it will break the type converter
            val fields = item.split("¦")
            //For all the fields, making sure their are the correct amount
            if (fields.size == 12) {
                //Set each field to its required type, making sure to account for nulls, if nulls occur set it to a defualt amount
                val id = fields[0].toInt()
                val name = fields[1]
                val targetMuscles = fields[2]
                val imagePath = fields[3]
                val weight1 = fields[4].toDoubleOrNull() ?: 0.0
                val weight2 = fields[5].toDoubleOrNull() ?: 0.0
                val weight3 = fields[6].toDoubleOrNull() ?: 0.0
                val reps = fields[7].toIntOrNull() ?: 0
                val sets = fields[8].toIntOrNull() ?: 0
                val dropSet = fields[9].toBoolean()
                val timePerSetInSeconds = fields[10].toIntOrNull() ?: 0
                val instructions = fields[11]

                //Make the exericse with all the values we extracted
                val exercise = Exercise(
                    id, name, targetMuscles, imagePath,
                    weight1, weight2, weight3, reps, sets, dropSet, timePerSetInSeconds, instructions
                )
                //Add the exercise to the list of exercises
                exerciseList.add(exercise)
            }
        }
        return exerciseList
    }

    @TypeConverter
    @JvmStatic
    //Converting a List<Exercise> to a String
    fun toExerciseList(list: List<Exercise>): String {
        //Split the exerciseList up, into its exercises, with a unlikey to be used characters
        return list.joinToString("¬") {
            //Split the exercise values up an unlikey to be used character
            "${it.id}¦${it.name}¦${it.targetMuscles}¦" +
                    "${it.imagePath}¦${it.weight1}¦${it.weight2}¦${it.weight3}¦" +
                    "${it.reps}¦${it.sets}¦${it.dropSet}¦${it.timePerSetInSeconds}¦${it.instructions}"
        }
    }
}