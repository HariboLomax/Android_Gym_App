package com.example.gym_application.ui.screens.home_screen
import android.annotation.SuppressLint
import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.DeleteForever
import androidx.compose.material3.ColorScheme
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.paint
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.example.gym_application.R
import com.example.gym_application.ui.components.MainPageNavigationBar
import com.example.gym_application.ui.components.MainPageTopAppBar
import com.example.gym_application.ui.model.Exercise
import com.example.gym_application.ui.model.ExerciseViewModel
import com.example.gym_application.ui.model.Session
import com.example.gym_application.ui.model.SessionViewModel

import com.example.gym_application.ui.navigation.Screen
import com.example.gym_application.ui.theme.Gym_ApplicationTheme
import java.time.LocalDate

@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
//Function to create the HomeScreen
fun HomeScreen(
    navController: NavHostController,
    exerciseViewModel: ExerciseViewModel,
    sessionViewModel: SessionViewModel
){
    //Mutable state to remeber if the drop down menu is open or not
    var expanded by remember { mutableStateOf(false) }
    //List of days IN ORDER
    var daysOfTheWeek = listOf<String>("Monday","Tuesday","Wednesday","Thursday","Friday","Saturday","Sunday")
    //Get all the sessions from the data base via the viewmodel
    val allSessions by sessionViewModel.allSessions.observeAsState(listOf())

    Column{
        Scaffold(
            topBar = {
                MainPageTopAppBar(stringResource(R.string.home))
            },
            bottomBar = {
                MainPageNavigationBar(navController)
            },
            //Floating action button to add a new session/day
            //If there are 7 days already (all days of the week) then dont appear, so they cant add more
            //If less than 7 days trigger the drop down menu to select the new day to add
            floatingActionButton = {
                    if (allSessions.size < 7) {
                        FloatingActionButton(
                            onClick = {
                                expanded = true
                            },
                        ) {
                            Icon(
                                imageVector = Icons.Filled.Add,
                                contentDescription = stringResource(R.string.add_session)
                            )
                        }
                    }
            },
            content = {
                innerPadding -> HomeContent(innerPadding, navController,sessionViewModel, daysOfTheWeek)
                //Drop down menu to provide the options that can be added to the session screen
                DropdownMenu(expanded = expanded, onDismissRequest = { expanded = false },modifier = Modifier.padding(15.dp)) {
                    //For every day in the day of the week list
                    for(day in daysOfTheWeek){
                        //if the session for that day doesnt already exist then make a button for it in the drop down
                        if (allSessions.find { it.day == day } == null){
                            //Colour scheme and backgroud colour
                            val colorScheme: ColorScheme = MaterialTheme.colorScheme
                            val surfaceColor: Color = colorScheme.surface
                            var backgroundColour = surfaceColor
                            //Button that is the currnet day will be highlighted
                            val date = LocalDate.now()
                            val dayString = date.dayOfWeek.toString()
                            //If it is the current day then make the background green
                            if (dayString == day.uppercase()){
                                backgroundColour = Color.Green
                            }
                            DropdownMenuItem(
                                text = { Text(day) },
                                //On click create the new blank session and add it to the session database via the sessionviewmodel
                                onClick = {
                                    val newList: MutableList<Exercise> = mutableListOf()
                                    val session = Session(
                                        0,
                                        day,
                                        newList,
                                        "[Session Name]"
                                    )
                                    sessionViewModel.addSession(session)
                                    expanded = false
                                }
                            )
                        }
                    }
                }
            }
        )


    }
}

@Composable
//Function to create the HomeScreenContent -> background, and all session tabs
private fun HomeContent(innerPadding: PaddingValues, navController: NavHostController, sessionViewModel: SessionViewModel, daysOfTheWeek : List<String>){
    Box(modifier = Modifier

        .padding(innerPadding)
        .fillMaxSize()
        .paint(
            painterResource(id = R.drawable.backgroundimage),
            contentScale = ContentScale.FillBounds,
        )
    )
    {
        Row{
            Column(modifier = Modifier
                .fillMaxSize()
                .fillMaxHeight()
                .verticalScroll(rememberScrollState(), true)
            ) {
                val allSessions by sessionViewModel.allSessions.observeAsState(listOf())
                val sortedSessions = allSessions.sortedBy { session ->
                    daysOfTheWeek.indexOf(session.day)
                }
                for(session in sortedSessions){
                    SessionTab(session = session, navController = navController, sessionViewModel)
                }
            }
        }
    }
}

@Composable
//Function to create a button for the session based on the one provided
fun SessionTab(session: Session,navController : NavHostController, sessionViewModel: SessionViewModel ){
    //Get all sessiosn from the databse via the viewmodel
    val allSessions by sessionViewModel.allSessions.observeAsState(listOf())
    //background colours and colour scheme
    val colorScheme: ColorScheme = MaterialTheme.colorScheme
    val surfaceDimColor: Color = colorScheme.surface
    var backgroundColour = surfaceDimColor.copy(0.8F)
    //If session is the current day then make it green
    if (session.isToday()){
        backgroundColour = Color.Green.copy(0.8F)
    }
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .size(100.dp)
            .scale(1F, 1F)
            .padding(5.dp)
            .background(
                color = backgroundColour,
                shape = RoundedCornerShape(15.dp, 15.dp, 15.dp, 15.dp)
            )
            .clickable {
                val sessionID = session.id
                navController.navigate("${Screen.SessionScreen.basePath}${sessionID}") {
                    popUpTo(navController.graph.findStartDestination().id)
                    launchSingleTop = true
                }
            }
            .border(5.dp, Color.Black, shape = RoundedCornerShape(15.dp, 15.dp, 15.dp, 15.dp))
        ,
        contentAlignment = Alignment.Center
    )
    {
        Row(){
            Box(modifier = Modifier
                .weight(0.3F)
                .align(alignment = CenterVertically)
                .padding(horizontal = 15.dp)
            ){Text(text = session.day,fontSize = 20.sp)}
            Box(modifier = Modifier
                .weight(0.3F)
                .align(alignment = CenterVertically)){Text(text = (session.getTotalTimeString()), fontSize = 20.sp)}
            Box(modifier = Modifier
                .weight(0.1F)
                .align(alignment = CenterVertically)){
                //If there is more than one session left then the delete button is visable
                if (allSessions.size > 1) {
                    IconButton(onClick = {
                        //Delete the session if there is more than one session
                        if (allSessions.size > 1) {
                            sessionViewModel.deleteSession(session)
                        }
                    }) {
                        Icon(Icons.Filled.DeleteForever, contentDescription = "Delete Day")
                    }
                }
            }
        }
    }
}



@Preview
@Composable
private fun HomeScreenPreview() {
    val navController = rememberNavController()
    val exerciseViewModel: ExerciseViewModel = viewModel()
    val sessionViewModel: SessionViewModel = viewModel()
    Gym_ApplicationTheme(dynamicColor = false) {
        HomeScreen(navController, exerciseViewModel, sessionViewModel)
    }
}
