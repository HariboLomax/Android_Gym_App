package com.example.gym_application.ui.screens.listofallexercises_screen

import android.annotation.SuppressLint
import android.net.Uri
import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material3.ColorScheme
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.paint
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.example.gym_application.R
import com.example.gym_application.ui.components.MainPageNavigationBar
import com.example.gym_application.ui.components.MainPageTopAppBar
import com.example.gym_application.ui.model.ExerciseViewModel
import com.example.gym_application.ui.navigation.Screen
import com.example.gym_application.ui.theme.Gym_ApplicationTheme
import kotlinx.coroutines.launch


@SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
@Composable
//Function to create the ListOfALlExerciseScreen
fun ListOfAllExercisesScreen(
    navController: NavHostController,
    exerciseViewModel: ExerciseViewModel
) {
    Scaffold(
        topBar = {
            MainPageTopAppBar("ListOfAllExercises")
        },
        bottomBar = {
            MainPageNavigationBar(navController)

        },
        //Floating action button to add a new exercise to the list
        floatingActionButton = {
            FloatingActionButton(
                //On click navigate to the add exercise form
                onClick = {
                    navController.navigate(Screen.AddExerciseForm.route) {
                        launchSingleTop = true
                    }
                },
            ) {
                Icon(
                    imageVector = Icons.Filled.Add,
                    contentDescription = "Add Exercise"
                )
            }
        },
        content = {  innerPadding -> ListOfAllExercisesContent(innerPadding,exerciseViewModel, navController) }
    )
}

@Composable
//Function to create the List of exercises main content -> the list of exercises
fun ListOfAllExercisesContent(innerPadding : PaddingValues, exerciseViewModel: ExerciseViewModel, navController: NavHostController){
    Box(
        modifier = Modifier
            .padding(innerPadding)
            .fillMaxSize()
            .fillMaxHeight()
            .paint(
                painterResource(id = R.drawable.backgroundimage),
                contentScale = ContentScale.FillBounds,
            )

    )
    {
        Column ( modifier = Modifier
            .fillMaxSize()
            .fillMaxHeight()
            .verticalScroll(rememberScrollState(), true)
            )
        {
            //Get all exercises from data base via the viewmodel
            val allExercises by exerciseViewModel.allExercises.observeAsState(listOf())
            //For each exercise make its own card
            for (exercise in allExercises) {
                ExerciseTab(name = exercise.name, image = exercise.imagePath, navController, exercise.id)
            }

        }
    }

}

@OptIn(ExperimentalGlideComposeApi::class)
@Composable
//Function to make a card for the provided exercise
fun ExerciseTab(name : String, image : String, navController: NavHostController, id : Int){
    val colorScheme: ColorScheme = MaterialTheme.colorScheme
    val surfaceDimColor: Color = colorScheme.surface
    var backgroundColour = surfaceDimColor.copy(0.8F)
    Box(
        modifier = Modifier
            .padding(5.dp)
            .fillMaxWidth()
            .scale(1F, 1F)
            //.weight(boxWeight)
            .background(
                color = backgroundColour,
                shape = RoundedCornerShape(15.dp, 15.dp, 15.dp, 15.dp)
            )
            //On click navigate to the exercise info screen with this exercise as a parameter
            .clickable {
                navController.navigate("${Screen.ExerciseInfoScreen.basePath}${id}") {
                    //popUpTo(navController.graph.findStartDestination().id)
                    launchSingleTop = true
                }
            }
            .border(
                5.dp,
                Color.Black, shape = RoundedCornerShape(15.dp, 15.dp, 15.dp, 15.dp)

            ),
        contentAlignment = Alignment.Center
    )
    {
        Column{
            Text(
                text = name,
                modifier = Modifier
                    .padding(15.dp)
                    .align(alignment = Alignment.CenterHorizontally),
                fontSize = 20.sp,
                style = TextStyle(textDecoration = TextDecoration.Underline)
            )
            GlideImage(
                model = Uri.parse(image),
                contentDescription = name,
                modifier = Modifier
                    .fillMaxWidth()
                    .size(300.dp)
                    .padding(15.dp)
            )
        }
    }
}

@Preview
@Composable
private fun ListOfAllScreenPreview() {
    val navController = rememberNavController()
    val exerciseViewModel: ExerciseViewModel = viewModel()
    Gym_ApplicationTheme(dynamicColor = false) {
        ListOfAllExercisesScreen(navController, exerciseViewModel)
    }
}