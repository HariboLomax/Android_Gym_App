package com.example.gym_application.ui.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.time.LocalDate

//Data Type for Exercise
@Entity(tableName = "exercises")
data class Exercise(
    @PrimaryKey(autoGenerate = true)
    val id : Int = 0,
    var name : String,
    var targetMuscles : String,
    var imagePath : String,
    var weight1: Double,
    var weight2 : Double,
    var weight3 : Double,
    var reps: Int,
    var sets: Int,
    var dropSet: Boolean,
    var timePerSetInSeconds : Int,
    var instructions : String
) {
}