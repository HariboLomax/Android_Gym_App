package com.example.gym_application.ui.components

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.navigation.NavHostController
import com.example.gym_application.ui.model.Exercise

@Composable
//Secondary Page Top App Bar is used give a title ADN a back button to each of the secondary screens.
//Since secondary screens dont have a mainpagenav they need a back button to get back to the previous screen
fun SecondaryScreenTopAppBar(navController: NavHostController, titleText : String){
    CenterAlignedTopAppBar(
        title = { Text(titleText) },
        navigationIcon = {
            IconButton(
                onClick = { navController.navigateUp() }
            ) {
                Icon(
                    imageVector = Icons.Default.ArrowBack,
                    contentDescription = null,
                    tint = MaterialTheme.colorScheme.primary
                )
            }
        }
    )
}