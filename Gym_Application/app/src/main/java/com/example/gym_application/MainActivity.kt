package com.example.gym_application

import android.app.Activity
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavType
import androidx.navigation.compose.rememberNavController
import com.example.gym_application.ui.screens.home_screen.HomeScreen
import com.example.gym_application.ui.theme.Gym_ApplicationTheme
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.example.gym_application.ui.screens.listofallexercises_screen.ListOfAllExercisesScreen
import com.example.gym_application.ui.model.ExerciseViewModel
import com.example.gym_application.ui.model.SessionViewModel
import com.example.gym_application.ui.navigation.Screen
import com.example.gym_application.ui.screens.exerciseinfo_screen.ExerciseInfoScreen
import com.example.gym_application.ui.screens.forms.AddExerciseForm
import com.example.gym_application.ui.screens.forms.EditExerciseForm
import com.example.gym_application.ui.screens.session_screen.SessionScreen

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {

            Gym_ApplicationTheme(dynamicColor = false) {
                // A surface container using the 'background' color from the theme
                Surface(modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background) {
                    BuildNavigationGraph()
                }
            }
        }
    }
}

@Composable
//Function to build the navigation graph
private fun BuildNavigationGraph(exerciseViewModel: ExerciseViewModel = viewModel(),sessionViewModel: SessionViewModel = viewModel()){
    val navController = rememberNavController();

    //Get all sessions and all exercises from their data bases
    val allSessions by sessionViewModel.allSessions.observeAsState(listOf())
    val allExercises by exerciseViewModel.allExercises.observeAsState(listOf())

    val defaultDay = 0//this is just a place holder as there will ALWAYS be a day in the sessions available
    var chosenDay by remember{ mutableStateOf(defaultDay)}

    val defaultExercise = 0 //this is just a place holder as you cant navigate to a screen that needs it if there are no exercises
    var chosenExerciseName_EditExercise by remember{ mutableStateOf(defaultExercise)}
    var chosenExerciseName_ExerciseInfo by remember{ mutableStateOf(defaultExercise)}

    var startDestination = remember { Screen.Home.route }

    val ctx = LocalContext.current as Activity
    Log.d("PACKAGE NAME",ctx.packageName)
    val viewExercise = stringResource(R.string.com_example_gym_application_action_view_exercises)
    val exerciseUri = stringResource(R.string.http_com_example_gym_application_exercises)

    ctx.intent?.let {
        if (it.action != null && it.action == viewExercise) {
            if (it.data != null && it.data.toString() == exerciseUri) {
                startDestination = Screen.ListOfAllExercises.route
            }
        }
    }

    NavHost(
        navController = navController,
        startDestination = startDestination
    ) {
        composable(Screen.Home.routePath()) { HomeScreen(navController,exerciseViewModel,sessionViewModel) }
        composable(Screen.ListOfAllExercises.routePath()) { ListOfAllExercisesScreen(navController,exerciseViewModel) }
        composable(Screen.AddExerciseForm.routePath()){ AddExerciseForm(navController,sessionViewModel,exerciseViewModel)}

        composable(Screen.SessionScreen.routePath(), arguments = listOf(navArgument(Screen.SessionScreen.arguement){type = NavType.IntType})){
            backStackEntry -> backStackEntry.arguments?.let{
                if (it.containsKey(Screen.SessionScreen.arguement)){
                    chosenDay = it.getInt(Screen.SessionScreen.arguement)
                }
            allSessions.find { it.id == chosenDay }
                ?.let { it1 -> SessionScreen(navController = navController, session = it1,sessionViewModel, exerciseViewModel) }
        }
        }

        composable(Screen.ExerciseInfoScreen.routePath(),arguments = (listOf(navArgument(Screen.ExerciseInfoScreen.arguement){type = NavType.IntType}))){
                backStackEntry -> backStackEntry.arguments?.let{
            if (it.containsKey(Screen.ExerciseInfoScreen.arguement)){
                chosenExerciseName_ExerciseInfo = it.getInt(Screen.ExerciseInfoScreen.arguement)
            }
            allExercises.find { it.id == chosenExerciseName_ExerciseInfo }
                ?.let { it1 -> ExerciseInfoScreen(navController = navController, exerciseSeleceted = it1,sessionViewModel,exerciseViewModel) }
        }
        }
        composable(Screen.EditExerciseForm.routePath(),arguments = (listOf(navArgument(Screen.EditExerciseForm.arguement){type = NavType.IntType}))){
                backStackEntry -> backStackEntry.arguments?.let{
            if (it.containsKey(Screen.EditExerciseForm.arguement)){
                chosenExerciseName_EditExercise = it.getInt(Screen.EditExerciseForm.arguement)
            }
            allExercises.find { it.id == chosenExerciseName_EditExercise }
                ?.let { it1 -> EditExerciseForm(navController = navController, sessionViewModel,exerciseViewModel,it1) }
        }
        }
    }
}


