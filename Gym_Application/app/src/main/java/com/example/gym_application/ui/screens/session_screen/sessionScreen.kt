package com.example.gym_application.ui.screens.session_screen

import android.util.Log

import androidx.compose.animation.core.updateTransition
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.DeleteForever
import androidx.compose.material.icons.filled.Save
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ColorScheme
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.paint
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.compose.rememberNavController
import com.example.gym_application.R
import com.example.gym_application.ui.components.SecondaryScreenTopAppBar
import com.example.gym_application.ui.model.Exercise
import com.example.gym_application.ui.model.ExerciseViewModel
import com.example.gym_application.ui.model.SecondsToHourFormat
import com.example.gym_application.ui.model.Session
import com.example.gym_application.ui.model.SessionViewModel
import com.example.gym_application.ui.navigation.Screen
import com.example.gym_application.ui.theme.Gym_ApplicationTheme

@Composable
//Function to create the SessionScreen
fun SessionScreen(navController: NavHostController, session: Session, sessionViewModel: SessionViewModel, exerciseViewModel : ExerciseViewModel){
    //Expand variable to keep track of the state of the dropdown menu
    var expanded by remember { mutableStateOf(false) }
    //Get all exercises from the data base via the viewmodel
    val allExercises by exerciseViewModel.allExercises.observeAsState(listOf())

    Column{
        Scaffold(
            topBar = {
                SecondaryScreenTopAppBar(navController,session.day.toString())
            },
            bottomBar = {

            },
            //Floating action button to bring up the list of exercises to add on click
            floatingActionButton = {
                FloatingActionButton(
                    onClick = {
                        expanded = true
                    },
                ) {
                    Icon(
                        imageVector = Icons.Filled.Add,
                        contentDescription = stringResource(R.string.add_session)
                    )
                }
            },
            content = {
                innerPadding -> SessionContent(innerPadding, session,sessionViewModel)
                //Drop down to provide the list of available exercises to add to the session
                DropdownMenu(expanded = expanded, onDismissRequest = { expanded = false },modifier = Modifier.padding(15.dp)) {
                    for (exercise in allExercises) {
                        //If adding the exercise would exceed 24hours then dont show it in the drop down
                        if((86400-session.calculateTotalTime()) > (exercise.timePerSetInSeconds * exercise.sets) ){
                            DropdownMenuItem(text = { Text(exercise.name)}, onClick = {
                                val updatedSession = session.addExercise(exercise)
                                sessionViewModel.updateSession(updatedSession)
                                expanded= false
                            })
                        }
                    }

                }
            }
        )


    }
}

@Composable
//Function to create the Session Screen Content -> the exercises available, the session name and total time
fun SessionContent(innerPadding : PaddingValues, session: Session,sessionViewModel: SessionViewModel){
    Box(
        modifier = Modifier
            .padding(innerPadding)
            .fillMaxSize()
            .fillMaxHeight()
            .paint(
                painterResource(id = R.drawable.backgroundimage),
                contentScale = ContentScale.FillBounds,
            )

    )
    {
        Column ( modifier = Modifier
            .fillMaxSize()
            .fillMaxHeight()
            .verticalScroll(rememberScrollState(), true)
        )
        {
            SessionDetailsBar(session, sessionViewModel)
            //For every exercise in the session create an exercise card
            //Added in the order they were added to the session
            for(exercise in session.exercises){
                ExerciseCard(innerPadding, exercise, session, sessionViewModel)
            }
        }
    }
}

@Composable
//Function to create the ExerciseCard
fun ExerciseCard(innerPadding : PaddingValues, exercise: Exercise,session : Session, sessionViewModel: SessionViewModel){
    //Background colour schemes
    val colorScheme: ColorScheme = MaterialTheme.colorScheme
    val surfaceDimColor: Color = colorScheme.surface
    var backgroundColour = surfaceDimColor.copy(0.8F)
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .scale(1F, 1F)
            .padding(5.dp)
            .background(
                color = backgroundColour,
                shape = RoundedCornerShape(15.dp, 15.dp, 15.dp, 15.dp)
            )
            .border(5.dp, Color.Black, shape = RoundedCornerShape(15.dp, 15.dp, 15.dp, 15.dp))
        ,
        contentAlignment = Alignment.Center
    )

    {
        Column(
            modifier = Modifier
                .fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ){
            //Create all the rows of information for the card
            //Doesn't include image to keep it concicse and easy to read during exercise
            NameAndTimeRow(exercise)
            WeightAndRepRow(exercise)
            //If there is a drop set there is 3 weights
            if(exercise.dropSet){
                Weight2AndWeight3Row(exercise)
            }
            SetsAndTimePerSetRow(exercise)
            DropSetRow(exercise)
            //IconButton for deleting the exercise from the session
            IconButton(onClick = {
                val updatedSession = session.removeExercise(exercise)
                sessionViewModel.updateSession(updatedSession)
            }
            ) {
                Icon(Icons.Filled.DeleteForever, stringResource(R.string.delete_exercise_from_sessiom))
            }

        }

    }
}

@Composable
//Function to create the Name Row
fun NameAndTimeRow(exercise: Exercise){
    Row(
        Modifier.fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center

    ){
        Text(exercise.name , modifier = Modifier
            .weight(0.4F)
            .padding(10.dp), fontWeight = FontWeight.Bold)
        Text("Aprox time: " + SecondsToHourFormat(exercise.timePerSetInSeconds*exercise.sets), modifier = Modifier
            .weight(0.6F)
            .padding(10.dp))
    }
}

@Composable
//Function to create the Weight1 and Rep Row
fun WeightAndRepRow(exercise: Exercise){
    Row(
        Modifier.fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center

    ){
        Text("Weight1 (Kg): "+exercise.weight1, modifier = Modifier
            .weight(0.6F)
            .padding(10.dp))
        Text("Reps : "+exercise.reps, modifier = Modifier
            .weight(0.4F)
            .padding(10.dp))
    }
}

@Composable
//Function to create the Weight 2 and 3 row
fun Weight2AndWeight3Row(exercise : Exercise){
    Row(
        Modifier.fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center

    ){
        Text("Weight2 (Kg): "+exercise.weight2, modifier = Modifier
            .weight(0.5F)
            .padding(10.dp))
        Text("Weight3 (Kg): "+exercise.weight3, modifier = Modifier
            .weight(0.5F)
            .padding(10.dp))

    }
}

@Composable
//Function to create sets and time per set row
fun SetsAndTimePerSetRow(exercise : Exercise){
    Row(
        Modifier.fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center

    ){
        Text("Sets : "+exercise.sets, modifier = Modifier
            .weight(0.4F)
            .padding(10.dp))
        Text("Time Per Set (seconds) : "+exercise.timePerSetInSeconds, modifier = Modifier
            .weight(0.6F)
            .padding(10.dp))

    }
}

@Composable
//Function to create the drop set row
fun DropSetRow(exercise: Exercise){
    Row(
        Modifier.fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center

    ) {
        Text("Dropset : ", modifier = Modifier
            .weight(0.4F)
            .padding(10.dp))
        Switch(
            checked = exercise.dropSet,
            onCheckedChange = {
            },
            modifier = Modifier
                .padding(8.dp)
                .weight(0.6F)
        )
    }
}


@Composable
//Function to create the top bar for the session -> contains the session title and the total time
fun SessionDetailsBar(session: Session, sessionViewModel: SessionViewModel){
    val colorScheme: ColorScheme = MaterialTheme.colorScheme
    val surfaceDimColor: Color = colorScheme.surface
    var backgroundColour = surfaceDimColor.copy(0.8F)
    Box(modifier = Modifier
        .padding(5.dp)
        .fillMaxWidth()
        .scale(1F, 1F)
        //.weight(boxWeight)
        .background(
            color = backgroundColour,
            shape = RoundedCornerShape(15.dp, 15.dp, 15.dp, 15.dp)
        )
        .border(
            5.dp,
            Color.Black, shape = RoundedCornerShape(15.dp, 15.dp, 15.dp, 15.dp)
        ),
        contentAlignment = Alignment.Center)
    {
        Row(){
            var textValue = remember{mutableStateOf(session.title.toString())}
            TextField(
                value = textValue.value,
                onValueChange = { textValue.value = it},
                modifier = Modifier
                    .align(alignment = Alignment.CenterVertically)
                    .weight(0.6F)
                    .padding(5.dp)
                    .background(
                        color = backgroundColour,
                        shape = RoundedCornerShape(15.dp, 15.dp, 15.dp, 15.dp)
                    )
            )
            //Iconbutton of to save the title on click
            IconButton(onClick = {
                session.title = textValue.value
                sessionViewModel.updateSession(session)
                                 }
                ,modifier = Modifier
                    .align(alignment = Alignment.CenterVertically)
                    .weight(0.1F)
                    .padding(5.dp)
            ) {
                Icon(
                    imageVector = Icons.Default.Save,
                    contentDescription = null,
                    tint = Color.Black
                )
            }
            Text(session.getTotalTimeString(), modifier = Modifier
                .align(alignment = Alignment.CenterVertically)
                .weight(0.3F)
                .padding(5.dp), fontSize = 20.sp)

        }
    }
}



@Preview
@Composable
private fun SessionScreenPreview() {
    val navController = rememberNavController()
    val sessionViewModel: SessionViewModel = viewModel()
    val exerciseViewModel : ExerciseViewModel = viewModel()
    val allSessions by sessionViewModel.allSessions.observeAsState(listOf())
    val defaultSession = allSessions[0]
    Gym_ApplicationTheme(dynamicColor = false) {
        SessionScreen(navController, defaultSession, sessionViewModel, exerciseViewModel)
    }
}
