package com.example.gym_application.ui.screens.exerciseinfo_screen

import android.net.Uri
import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.DeleteForever
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material3.ColorScheme
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.paint
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Popup
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.example.gym_application.R
import com.example.gym_application.ui.components.SecondaryScreenTopAppBar
import com.example.gym_application.ui.model.Exercise
import com.example.gym_application.ui.model.ExerciseViewModel
import com.example.gym_application.ui.model.SecondsToHourFormat
import com.example.gym_application.ui.model.Session
import com.example.gym_application.ui.model.SessionViewModel
import com.example.gym_application.ui.navigation.Screen
import com.example.gym_application.ui.theme.Gym_ApplicationTheme
import kotlinx.coroutines.launch


//Function to create the base scaffold of the screen and then to call the individual components
@Composable
fun ExerciseInfoScreen(navController: NavHostController, exerciseSeleceted : Exercise, sessionViewModel: SessionViewModel, exerciseViewModel: ExerciseViewModel){

    Column{
        Scaffold(
            topBar = {
                SecondaryScreenTopAppBar(navController,exerciseSeleceted.name+" Details")
            },
            bottomBar = {

            },
            content = {
                    innerPadding -> ExerciseInfo(innerPadding, exerciseSeleceted,sessionViewModel,exerciseViewModel,navController)

            }
        )


    }
}

//Function to create the container that holds all the information for the selected exercise
@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun ExerciseInfo(innerPadding : PaddingValues, exerciseSeleceted: Exercise, sessionViewModel: SessionViewModel, exerciseViewModel: ExerciseViewModel,navController:NavHostController){
    //Gets all the sessions from the data base
    val allSessions by sessionViewModel.allSessions.observeAsState(listOf())
    //if the exercise is currently in use
    var currentlyInSession = false
    //the sessions the exercise is in use in, defualt : 0
    var sessionCurrentyIn : Session = allSessions[0]

    //Go through every session and check if the exercise is in use
    //If it is update the corresponding values
    for(session in allSessions){
        for (exercise in session.exercises){
            if (exercise.id == exerciseSeleceted.id){
                currentlyInSession = true
                sessionCurrentyIn = session
            }
        }
    }

    //Mutable state for the popup being active
    var exerciseInUsePopUp by remember{ mutableStateOf(false) }

    //Colours so that they match the colour scheme
    val colorScheme: ColorScheme = MaterialTheme.colorScheme
    val surfaceDimColor: Color = colorScheme.surface
    var backgroundColour = surfaceDimColor.copy(0.8F)
    //Background Box with background image
    Box(
        modifier = Modifier
            .padding(innerPadding)
            .fillMaxSize()
            .fillMaxHeight()
            .paint(
                painterResource(id = R.drawable.backgroundimage),
                contentScale = ContentScale.FillBounds,
            )
    )
    {
        //Box to contain the exercise information
        Box(modifier = Modifier
            .padding(5.dp)
            .fillMaxWidth()
            .scale(1F, 1F)
            //Setting background colour and rounding corners
            .background(
                color = backgroundColour,
                shape = RoundedCornerShape(15.dp, 15.dp, 15.dp, 15.dp)
            )
            //Ading black border round the box
            .border(
                5.dp,
                Color.Black, shape = RoundedCornerShape(15.dp, 15.dp, 15.dp, 15.dp)
            ),
            contentAlignment = Alignment.Center)
        {
            Column( modifier = Modifier
                .fillMaxSize()
                .fillMaxHeight()
                .verticalScroll(rememberScrollState(), true)
            )
            {
                val fontSize = 20.sp
                //Title
                Text(text = exerciseSeleceted.name,fontSize = 30.sp, modifier = Modifier
                    .padding(5.dp)
                    .align(alignment = Alignment.CenterHorizontally),style = TextStyle(textDecoration = TextDecoration.Underline))
                //DropSet
                if(!exerciseSeleceted.dropSet) {
                    Text(text = stringResource(R.string.not_a_drop_set),fontSize = fontSize, modifier = Modifier
                        .padding(10.dp)
                        .align(alignment = Alignment.CenterHorizontally),style = TextStyle(fontWeight = FontWeight.Bold))
                }
                else{
                    Text(text = stringResource(R.string.a_drop_set),fontSize = fontSize, modifier = Modifier
                        .padding(10.dp)
                        .align(alignment = Alignment.CenterHorizontally),style = TextStyle(fontWeight = FontWeight.Bold))
                }
                //Target Muscles
                Text(text = "Target Muscles:\n  "+exerciseSeleceted.targetMuscles,fontSize = fontSize, modifier = Modifier.padding(10.dp))
                //DROP SET
                if(!exerciseSeleceted.dropSet){
                    Text(text = "Weight : "+exerciseSeleceted.weight1.toString()+ "Kg",fontSize = fontSize, modifier = Modifier.padding(10.dp))
                }else{
                    Text(text = "Weight1 : " + exerciseSeleceted.weight1.toString()+ "Kg",fontSize = fontSize,modifier = Modifier.padding(5.dp))
                    Text(text = "Weight2 : " + exerciseSeleceted.weight2.toString()+ "Kg",fontSize = fontSize,modifier = Modifier.padding(5.dp))
                    Text(text = "Weight3 : " + exerciseSeleceted.weight3.toString()+ "Kg",fontSize = fontSize,modifier = Modifier.padding(5.dp))
                }
                //Reps
                Text(text = "Reps : "+ exerciseSeleceted.reps.toString(),fontSize = fontSize, modifier = Modifier.padding(10.dp))
                //Sets
                Text(text = "Sets : "+exerciseSeleceted.sets.toString(),fontSize = fontSize, modifier = Modifier.padding(10.dp))
                //time per set (seconds)
                Text(text = "Time Per Set (Seconds) : " + exerciseSeleceted.timePerSetInSeconds.toString() + "s",fontSize = fontSize, modifier = Modifier.padding(10.dp))
                //total time hours
                val totalTime = SecondsToHourFormat(exerciseSeleceted.timePerSetInSeconds*exerciseSeleceted.sets)
                Text(text = "Total time : " + totalTime,fontSize = fontSize, modifier = Modifier.padding(10.dp))
                //Image
                GlideImage(
                    model = Uri.parse(exerciseSeleceted.imagePath),
                    contentDescription = exerciseSeleceted.name,
                    modifier = Modifier
                        .fillMaxWidth()
                        .size(300.dp)
                        .padding(15.dp)
                )
                //Instructions
                Text(text = stringResource(R.string.instructions),fontSize = fontSize, modifier = Modifier.padding(10.dp),style = TextStyle(textDecoration = TextDecoration.Underline))
                Text(text = exerciseSeleceted.instructions,fontSize = fontSize,modifier = Modifier
                    .padding(5.dp)
                    .align(alignment = Alignment.CenterHorizontally))
                //Button Row
                Row(horizontalArrangement = Arrangement.Center,
                    verticalAlignment = Alignment.CenterVertically){
                    //Edit buttons
                    //On click navigates to the editExerciseForm screen
                        IconButton(onClick = {
                            val exerciseID = exerciseSeleceted.id
                            navController.navigate("${Screen.EditExerciseForm.basePath}${exerciseID}") {
                                popUpTo(navController.graph.findStartDestination().id)
                                launchSingleTop = true
                            }
                                             },modifier = Modifier
                            .weight(0.5F)
                            .padding(10.dp)) {

                            Icon(Icons.Filled.Edit, contentDescription = stringResource(R.string.edit_exercise), modifier = Modifier.size(40.dp))
                        }
                    //Delete button
                    //on click deletes the selected exercise
                    //If the exercise is in use make the popup visible
                        IconButton(
                            onClick = {

                                if (currentlyInSession == false) {
                                    //Then Delete it
                                    Log.d("ATTEMPTING TO DELETE: " + exerciseSeleceted.name, "Attempint to Delete")
                                    navController.navigateUp()
                                    exerciseViewModel.deleteExercise(exerciseSeleceted)

                                }else {
                                    exerciseInUsePopUp =
                                        true//IF NOT IN A SESSION THEN DELETE!, IF IN SESSION POPUP!
                                }

                                      },modifier = Modifier
                                .weight(0.5F)
                                .padding(10.dp)) {
                            Icon(Icons.Filled.DeleteForever, contentDescription = stringResource(R.string.delete_exercise), modifier = Modifier.size(40.dp))
                        }
                }
                //If exercise is in use popup warning that it is use
                //Tells the user which day has the exercise in use
                if(exerciseInUsePopUp){
                    Popup(
                        alignment = Alignment.CenterStart,
                        offset = IntOffset(0, 700),
                    ){
                        Box(
                            modifier = Modifier
                                .background(
                                    color = MaterialTheme.colorScheme.error,
                                    shape = RoundedCornerShape(15.dp, 15.dp, 15.dp, 15.dp)
                                )
                                .border(
                                    5.dp,
                                    Color.Black,
                                    shape = RoundedCornerShape(15.dp, 15.dp, 15.dp, 15.dp)
                                )
                                .fillMaxWidth()
                                .size(200.dp)
                                .padding(20.dp)
                        ){
                            Column(
                                modifier = Modifier
                                    .fillMaxSize(),
                                horizontalAlignment = Alignment.CenterHorizontally,
                                verticalArrangement = Arrangement.Center
                            ){
                                Text(text = stringResource(R.string.cannot_delete_exercise_warning), fontSize = 25.sp, modifier = Modifier.align(alignment = Alignment.CenterHorizontally),color = Color.White,fontWeight = FontWeight.Bold,textAlign = TextAlign.Center)
                                Text(text = "Exercise is in use on "+sessionCurrentyIn.day, fontSize = 20.sp, modifier = Modifier.align(alignment = Alignment.CenterHorizontally),color = Color.White,fontWeight = FontWeight.Bold,textAlign = TextAlign.Center)
                                IconButton(onClick = {
                                    exerciseInUsePopUp = false

                                }) {
                                    Icon(Icons.Filled.Close, contentDescription = stringResource(R.string.exercise_in_use), modifier = Modifier
                                        .size(40.dp)
                                        .align(alignment = Alignment.CenterHorizontally),tint = Color.White)
                                }
                            }

                        }
                    }
                }
            }
        }
    }
}





@Preview
@Composable
private fun ExerciseInfoScreenPreview() {
    val navController = rememberNavController()
    val sessionViewModel: SessionViewModel = viewModel()
    val exerciseViewModel : ExerciseViewModel = viewModel()
    val allExercises by exerciseViewModel.allExercises.observeAsState(listOf())
    val defaultExercise = allExercises[0]
    Gym_ApplicationTheme(dynamicColor = false) {
        ExerciseInfoScreen(navController, defaultExercise, sessionViewModel,exerciseViewModel)
    }
}