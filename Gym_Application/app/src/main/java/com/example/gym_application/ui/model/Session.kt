package com.example.gym_application.ui.model

import androidx.compose.ui.text.capitalize
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.example.gym_application.ui.datasource.util.ExerciseList_Converter
import java.time.LocalDate
import java.time.temporal.ChronoUnit

//Session Datatype
@Entity(tableName = "sessions")
data class Session(
    @PrimaryKey(autoGenerate = true)
    val id:Int = 0,
    val day : String,
    @TypeConverters(ExerciseList_Converter::class)
    var exercises : List<Exercise>,
    var title : String
)
{
    //Function to return if the session is the current day -> used to highlight the active day
    fun isToday() : Boolean{
        val date = LocalDate.now()
        val dayString = date.dayOfWeek.toString()
        print(dayString)
        return dayString == day.uppercase()
    }

    //Function to calculate the total amount of seconds in one sessions
    //Adds up all the seperate times of all the exercises
    fun calculateTotalTime() : Double{
        var totalTimeSeconds = 0.0
        for(exercise in exercises){
            totalTimeSeconds += exercise.timePerSetInSeconds * exercise.sets
        }
        return totalTimeSeconds
    }

    //Function to calculate the total time in the format x hours x minutes x seconds
    fun getTotalTimeString(): String {
        var totalTimeSeconds = 0
        for(exercise in exercises){
            totalTimeSeconds += exercise.timePerSetInSeconds * exercise.sets
        }
        return SecondsToHourFormat(totalTimeSeconds)
    }

    //Function to add an exercise to the exercise List
    fun addExercise(newExercise: Exercise): Session {
        val updatedExercises = exercises.toMutableList().apply { add(newExercise) }
        return copy(exercises = updatedExercises)
    }

    //Function to remove an exercise from the exercise List
    fun removeExercise(exerciseToRemove: Exercise): Session {
        val updatedExercises = exercises.toMutableList().apply { remove(exerciseToRemove) }
        return copy(exercises = updatedExercises)
    }

}