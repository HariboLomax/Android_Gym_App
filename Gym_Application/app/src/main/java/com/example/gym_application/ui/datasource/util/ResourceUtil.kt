package com.example.gym_application.ui.datasource.util

import android.content.Context
import android.os.Environment
import android.util.Log
import java.io.File
import java.text.SimpleDateFormat
import java.util.Date


private const val gym_application_Tag = "GYMAPPLICATION"

object ResourceUtil {

    fun getPhotoFileUri(context: Context, fileName: String): File {
        val mediaStorageDir: File =
            File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), gym_application_Tag)

        if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) {
            Log.d(gym_application_Tag, "failed to create directory")
        }

        return File(mediaStorageDir.path + File.separator + fileName)
    }

    fun createImageFile(context: Context): File {
        // Code obtained and adapted from: https://developer.android.com/training/camera/photobasics
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)

        return File.createTempFile(
            imageFileName,  /* prefix */
            ".jpg",  /* suffix */
            storageDir /* directory */
        )
    }

}