package com.example.gym_application.ui.screens.forms

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.MediaStore
import android.widget.Toast
import androidx.activity.compose.ManagedActivityResultLauncher
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.filled.Lock
import androidx.compose.material3.ColorScheme
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.paint
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.content.FileProvider
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.example.gym_application.R
import com.example.gym_application.ui.components.SecondaryScreenTopAppBar
import com.example.gym_application.ui.datasource.util.ResourceUtil
import com.example.gym_application.ui.model.Exercise
import com.example.gym_application.ui.model.ExerciseViewModel
import com.example.gym_application.ui.model.SecondsToHourFormat
import com.example.gym_application.ui.model.SessionViewModel
import com.example.gym_application.ui.theme.Gym_ApplicationTheme
import java.io.File
import java.io.IOException

@Composable
//Function to create a form to fill in to add a new exercise
fun AddExerciseForm(navController: NavHostController,sessionViewModel: SessionViewModel, exerciseViewModel: ExerciseViewModel){
    //Background colour
    val colorScheme: ColorScheme = MaterialTheme.colorScheme
    val surfaceDimColor: Color = colorScheme.surface
    var backgroundColour = surfaceDimColor.copy(0.8F)
    var namebackgroundColour = surfaceDimColor.copy(0.8F)

    //Default values for the form
    val id = 0
    var name = remember{(mutableStateOf("Default Name"))}
    var targetmuscles = remember { (mutableStateOf("")) }
    var weight1 = remember{(mutableStateOf(0.0))}
    var weight2  = remember{(mutableStateOf(0.0))}
    var weight3 = remember{(mutableStateOf(0.0))}
    var reps =  remember{(mutableStateOf(0))}
    var sets =  remember{(mutableStateOf(1))}
    var dropset = remember { mutableStateOf(false) }
    var timeperset =  remember{(mutableStateOf(1))}
    var instructions = remember{(mutableStateOf(""))}
    var imagepath = remember{ mutableStateOf("file:///android_asset/images/defaultimage.jpg")}

    //Form
    Column{
        Scaffold(
            topBar = {
                SecondaryScreenTopAppBar(navController,"Add Exercise")
            },
            floatingActionButton = {
                //Floating action point button
                //On selecting saves the current exercise with the infromation entered in the form
                FloatingActionButton(
                    onClick = {
                        SaveNewExercise(name,dropset,targetmuscles,weight1,weight2,weight3,reps,sets,timeperset,instructions,imagepath, exerciseViewModel)
                        navController.navigateUp()
                    },
                ) {
                    Icon(
                        imageVector = Icons.Filled.Check,
                        contentDescription = stringResource(R.string.add_exercise)
                    )
                }
            },
            bottomBar = {

            },
            content = {
                    innerPadding ->
                //Background box with background image
                Box(
                    modifier = Modifier
                        .padding(innerPadding)
                        .fillMaxSize()
                        .fillMaxHeight()
                        .paint(
                            painterResource(id = R.drawable.backgroundimage),
                            contentScale = ContentScale.FillBounds,
                        )

                )
                {
                    //Box to hold all the contents of the form
                    Box(modifier = Modifier
                        .padding(5.dp)
                        .fillMaxWidth()
                        .scale(1F, 1F)
                        //.weight(boxWeight)
                        .background(
                            color = backgroundColour,
                            shape = RoundedCornerShape(15.dp, 15.dp, 15.dp, 15.dp)
                        )
                        .border(
                            5.dp,
                            Color.Black, shape = RoundedCornerShape(15.dp, 15.dp, 15.dp, 15.dp)
                        ),
                        contentAlignment = Alignment.Center)
                    {
                        Column(modifier = Modifier
                            .fillMaxSize()
                            .fillMaxHeight()
                            .verticalScroll(rememberScrollState(), true),
                            horizontalAlignment = Alignment.CenterHorizontally
                        ){
                            //Functions to add all the corresponding rows to the form
                            NameRow(name)
                            DropSetSwitch(dropset) //adds the switch to toggle on/off dropset
                            TargetMuscleRow(targetmuscles)
                            WeightsRow(dropset,weight1,weight2,weight3)
                            RepsRow(reps)
                            SetsRow(dropset, sets)
                            TimePerSetRow(timeperset,sets)
                            TotalTimeRow(timeperset, sets)
                            ExerciseImage(imagepath)
                            InstructionsRow(instructions)
                        }
                    }
                }

            }
        )
    }
}


//Function to create the row of entry for name
@Composable
fun NameRow(name: MutableState<String>){
    //Name Row
    Row(){
        Text(text = "Exercise Name: ",
            modifier = Modifier
                .align(alignment = Alignment.CenterVertically)
                .weight(0.4F)
                .padding(10.dp)
        )
        TextField(
            value = name.value,
            onValueChange = {name.value = it},
            modifier = Modifier
                .align(alignment = Alignment.CenterVertically)
                .weight(0.6F)
                .padding(10.dp)

        )
    }
}

//Function to create the dropsetswitch
@Composable
fun DropSetSwitch(dropset : MutableState<Boolean>){
    //Drop Set
    Row() {
        //DropSetSwitch
        Text(text = "Drop Set: ",modifier = Modifier
            .align(alignment = Alignment.CenterVertically)
            .weight(0.4F)
            .padding(10.dp))
        Switch(
            checked = dropset.value,
            onCheckedChange = { dropset.value = it },
            modifier = Modifier
                .padding(10.dp)
                .weight(0.6F)
        )
    }
}

//Function to create the row of entry for muscles
@Composable
fun TargetMuscleRow(targetmuscles : MutableState<String>){
    //Target Muscles
    Row(){
        Text(text = "Target Muscles: ",
            modifier = Modifier
                .align(alignment = Alignment.CenterVertically)
                .weight(0.4F)
                .padding(10.dp)
        )
        TextField(
            value = targetmuscles.value,
            onValueChange = {targetmuscles.value = it},
            modifier = Modifier
                .align(alignment = Alignment.CenterVertically)
                .weight(0.6F)
                .padding(10.dp)

        )
    }
}

//Function to create the row of entry for weights
//Changes based on if it is a drop set or not -> drop set has 3 weights not 1
@Composable
fun WeightsRow(dropset: MutableState<Boolean>, weight1 : MutableState<Double>, weight2: MutableState<Double>, weight3: MutableState<Double>){
    if(dropset.value == false){//There is only one weight
        Row(){
            Text(text = "Weight1 (Kg): ",
                modifier = Modifier
                    .align(alignment = Alignment.CenterVertically)
                    .weight(0.4F)
                    .padding(10.dp)
            )
            TextField(
                value = weight1.value.toString(),
                onValueChange = {
                    if (it.toDoubleOrNull() != null && it.toDouble() > 0){
                        weight1.value = it.toDouble()
                    }
                },
                modifier = Modifier
                    .align(alignment = Alignment.CenterVertically)
                    .weight(0.6F)
                    .padding(10.dp)

            )
        }
    }else{
        Row() {
            Text(
                text = "Weight1 (Kg): ",
                modifier = Modifier
                    .align(alignment = Alignment.CenterVertically)
                    .weight(0.4F)
                    .padding(10.dp)
            )
            TextField(
                value = weight1.value.toString(),
                onValueChange = {
                    if (it.toDoubleOrNull() != null) {
                        weight1.value = it.toDouble()
                    }
                },
                modifier = Modifier
                    .align(alignment = Alignment.CenterVertically)
                    .weight(0.6F)
                    .padding(10.dp)

            )
        }
        Row() {
            Text(
                text = "Weight2 (Kg): ",
                modifier = Modifier
                    .align(alignment = Alignment.CenterVertically)
                    .weight(0.4F)
                    .padding(10.dp)
            )
            TextField(
                value = weight2.value.toString(),
                onValueChange = {
                    if (it.toDoubleOrNull() != null && it.toDouble() < weight1.value) {
                        weight2.value = it.toDouble()
                    }
                },
                modifier = Modifier
                    .align(alignment = Alignment.CenterVertically)
                    .weight(0.6F)
                    .padding(10.dp)

            )
        }
        Row() {
            Text(
                text = "Weight3 (Kg): ",
                modifier = Modifier
                    .align(alignment = Alignment.CenterVertically)
                    .weight(0.4F)
                    .padding(10.dp)
            )
            TextField(
                value = weight3.value.toString(),
                onValueChange = {
                    if (it.toDoubleOrNull() != null && it.toDouble() < weight2.value) {
                        weight3.value = it.toDouble()
                    }
                },
                modifier = Modifier
                    .align(alignment = Alignment.CenterVertically)
                    .weight(0.6F)
                    .padding(10.dp)

            )
        }

    }

}

//Function to create the row of entry for reps
@Composable
fun RepsRow(reps : MutableState<Int>){
    Row() {
        Text(
            text = "Reps: ",
            modifier = Modifier
                .align(alignment = Alignment.CenterVertically)
                .weight(0.4F)
                .padding(10.dp)
        )
        TextField(
            value = reps.value.toString(),
            onValueChange = {
                if (it.toIntOrNull() != null) {
                    reps.value = it.toInt()
                }
            },
            modifier = Modifier
                .align(alignment = Alignment.CenterVertically)
                .weight(0.6F)
                .padding(10.dp)

        )
    }
}

//Function to create the row of entry for sets
//Changes based on dropset -> drop set can only have 3 sets
@Composable
fun SetsRow(dropset: MutableState<Boolean>, sets: MutableState<Int>){
    if (dropset.value == true){
        Row() {
            Text(
                text = "Sets: ",
                modifier = Modifier
                    .align(alignment = Alignment.CenterVertically)
                    .weight(0.4F)
                    .padding(10.dp)
            )
            Text(
                text = "3",
                modifier = Modifier
                    .align(alignment = Alignment.CenterVertically)
                    .weight(0.3F)
                    .padding(10.dp)
            )
            Icon(Icons.Filled.Lock, stringResource(R.string.setslocked), modifier = Modifier
                .padding(10.dp)
                .weight(0.3F))
            sets.value = 3
        }
    }else{
        Row() {
            Text(
                text = "Sets: ",
                modifier = Modifier
                    .align(alignment = Alignment.CenterVertically)
                    .weight(0.4F)
                    .padding(10.dp)
            )
            TextField(
                value = sets.value.toString(),
                onValueChange = {
                    if (it.toIntOrNull() != null && it.toInt() > 0) {
                        sets.value = it.toInt()
                    }
                },
                modifier = Modifier
                    .align(alignment = Alignment.CenterVertically)
                    .weight(0.6F)
                    .padding(10.dp)
            )
        }
    }
}

//Function to create the row of entry for time per set
@Composable
fun TimePerSetRow(timeperset: MutableState<Int>, sets : MutableState<Int>){
    Row() {
        Text(
            text = "Time Per Set (Seconds): ",
            modifier = Modifier
                .align(alignment = Alignment.CenterVertically)
                .weight(0.4F)
                .padding(10.dp)
        )
        TextField(
            value = timeperset.value.toString(),
            onValueChange = {
                if (it.toIntOrNull() != null && it.toInt() > 0 && it.toInt() < (86400/sets.value)) {
                    timeperset.value = it.toInt()
                }
            },
            modifier = Modifier
                .align(alignment = Alignment.CenterVertically)
                .weight(0.6F)
                .padding(10.dp)
        )
    }
}

//Function to display the total amount of time of the exercise based on exercisestimeperset * no of sets
@Composable
fun TotalTimeRow(timeperset: MutableState<Int>, sets : MutableState<Int>){
    Row() {
        Text(
            text = "Total Time : ",
            modifier = Modifier
                .align(alignment = Alignment.CenterVertically)
                .weight(0.4F)
                .padding(10.dp)
        )
        Text(
            text = SecondsToHourFormat((timeperset.value*sets.value)),
            modifier = Modifier
                .align(alignment = Alignment.CenterVertically)
                .weight(0.3F)
                .padding(10.dp)
        )
    }
}

//Function to create the row of entry for instructions
@Composable
fun InstructionsRow(instructions : MutableState<String>){
    Row() {
        Text(
            text = "Instructions: ",
            modifier = Modifier
                .align(alignment = Alignment.CenterVertically)
                .weight(0.4F)
                .padding(10.dp)
        )
        TextField(
            value = instructions.value,
            onValueChange = {
                instructions.value = it
            },
            modifier = Modifier
                .align(alignment = Alignment.CenterVertically)
                .weight(0.6F)
                .padding(10.dp)
        )
    }

}

//Function to create the exercise image
//Selecting the iamge will take you to the camera to take your own image
@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun ExerciseImage(imagepath : MutableState<String>){
    var photoFile : File? = remember {null}
    val ctx = LocalContext.current
    val resultLauncher =
        rememberLauncherForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) {
                imagepath.value = "file://${photoFile!!.absolutePath}"
            }
        }
    Column(
        modifier = Modifier.padding(15.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center)
    {
        GlideImage(
            model = Uri.parse(imagepath.value),
            contentDescription = stringResource(R.string.exerciseimage),
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .height(200.dp)
                .clickable {
                    takePicture(
                        ctx = ctx,
                        resultLauncher = resultLauncher,
                    ) {
                        photoFile = it
                    }
                }
        )
        Text(text = stringResource(id = R.string.to_to_upload_image))
    }
}

private fun takePicture(ctx: Context, resultLauncher: ManagedActivityResultLauncher<Intent, androidx.activity.result.ActivityResult>, updateFile: (File) -> Unit){
    val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
    var photoFile: File? = null

    // Create the File where the photo should go
    try {
        photoFile = ResourceUtil.createImageFile(ctx)
    } catch (ex: IOException) {
        // Error occurred while creating the File
        Toast.makeText(
            ctx,
            ctx.getString(R.string.cannot_take_a_picture_there_may_not_be_enough_storage_space),
            Toast.LENGTH_SHORT
        ).show()
    }
    // Continue only if the File was successfully created
    photoFile?.let {
        val photoUri = FileProvider.getUriForFile(
            ctx,
            "com.example.gym_application",
            it
        )
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
        takePictureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
        try {
            resultLauncher.launch(takePictureIntent)
            updateFile(photoFile)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(ctx, R.string.cannot_take_a_picture_there_may_not_be_enough_storage_space, Toast.LENGTH_LONG)
                .show()
        }
    }

}
//Function to save the new exercise
fun SaveNewExercise(name : MutableState<String>,
                 dropset : MutableState<Boolean>,
                 targetmuscles : MutableState<String>,
                 weight1 : MutableState<Double>,
                 weight2 : MutableState<Double>,
                 weight3 : MutableState<Double>,
                 reps : MutableState<Int>,
                 sets : MutableState<Int>,
                 timeperset : MutableState<Int>,
                 instructions : MutableState<String>,
                 imagepath : MutableState<String>, exerciseViewModel : ExerciseViewModel){
    //Create the new exercise and populate it with the data provided
    val exercise = Exercise(
        0,
        name.value,
        targetmuscles.value,
        imagepath.value,
        weight1.value,
        weight2.value,
        weight3.value,
        reps.value,
        sets.value,
        dropset.value,
        timeperset.value,
        instructions.value
    )
    //add it to the data base through the viewmodel
    exerciseViewModel.addExercise(exercise)

}



@Preview
@Composable
private fun AddExerciseFormPreview() {
    val navController = rememberNavController()
    val sessionViewModel: SessionViewModel = viewModel()
    val exerciseViewModel : ExerciseViewModel = viewModel()
    val allExercises by exerciseViewModel.allExercises.observeAsState(listOf())
    Gym_ApplicationTheme(dynamicColor = false) {
        AddExerciseForm(navController, sessionViewModel,exerciseViewModel)
    }
}