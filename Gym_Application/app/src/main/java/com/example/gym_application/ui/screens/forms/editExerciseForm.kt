package com.example.gym_application.ui.screens.forms



import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.MediaStore
import android.widget.Toast
import androidx.activity.compose.ManagedActivityResultLauncher
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.filled.Lock
import androidx.compose.material3.ColorScheme
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.paint
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.core.content.FileProvider
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.example.gym_application.R
import com.example.gym_application.ui.components.SecondaryScreenTopAppBar
import com.example.gym_application.ui.datasource.util.ResourceUtil
import com.example.gym_application.ui.model.Exercise
import com.example.gym_application.ui.model.ExerciseViewModel
import com.example.gym_application.ui.model.SecondsToHourFormat
import com.example.gym_application.ui.model.SessionViewModel
import com.example.gym_application.ui.theme.Gym_ApplicationTheme
import java.io.File
import java.io.IOException

@Composable
//Function to edit an existing exercise
//Similar to creating an exercise, but populated with existing data
fun EditExerciseForm(navController: NavHostController,sessionViewModel: SessionViewModel, exerciseViewModel: ExerciseViewModel, exercise: Exercise){
    val allSessions by sessionViewModel.allSessions.observeAsState(listOf())
    val allExercises by exerciseViewModel.allExercises.observeAsState(listOf())
    val colorScheme: ColorScheme = MaterialTheme.colorScheme
    val surfaceDimColor: Color = colorScheme.surface
    var backgroundColour = surfaceDimColor.copy(0.8F)
    var namebackgroundColour = surfaceDimColor.copy(0.8F)
    val id = exercise.id
    var name = remember{(mutableStateOf(exercise.name))}
    var targetmuscles = remember { (mutableStateOf(exercise.targetMuscles)) }
    var weight1 = remember{(mutableStateOf(exercise.weight1))}
    var weight2  = remember{(mutableStateOf(exercise.weight2))}
    var weight3 = remember{(mutableStateOf(exercise.weight3))}
    var reps =  remember{(mutableStateOf(exercise.reps))}
    var sets =  remember{(mutableStateOf(exercise.sets))}
    var dropset = remember { mutableStateOf(exercise.dropSet) }
    var timeperset =  remember{(mutableStateOf(exercise.timePerSetInSeconds))}
    var instructions = remember{(mutableStateOf(exercise.instructions))}
    var imagepath = remember{ mutableStateOf(exercise.imagePath)}
    Column{
        Scaffold(
            topBar = {
                SecondaryScreenTopAppBar(navController,"Edit Exercise")
            },
            floatingActionButton = {
                FloatingActionButton(
                    onClick = {
                        UpdateExistingExercise(name,dropset,targetmuscles,weight1,weight2,weight3,reps,sets,timeperset,instructions,imagepath, exerciseViewModel, exercise)
                        navController.navigateUp()
                    },
                ) {
                    Icon(
                        imageVector = Icons.Filled.Check,
                        contentDescription = stringResource(R.string.add_exercise)
                    )
                }
            },
            bottomBar = {

            },
            content = {
                    innerPadding ->
                Box(
                    modifier = Modifier
                        .padding(innerPadding)
                        .fillMaxSize()
                        .fillMaxHeight()
                        .paint(
                            painterResource(id = R.drawable.backgroundimage),
                            contentScale = ContentScale.FillBounds,
                        )

                )
                {
                    Box(modifier = Modifier
                        .padding(5.dp)
                        .fillMaxWidth()
                        .scale(1F, 1F)
                        //.weight(boxWeight)
                        .background(
                            color = backgroundColour,
                            shape = RoundedCornerShape(15.dp, 15.dp, 15.dp, 15.dp)
                        )
                        .border(
                            5.dp,
                            Color.Black, shape = RoundedCornerShape(15.dp, 15.dp, 15.dp, 15.dp)
                        ),
                        contentAlignment = Alignment.Center)
                    {
                        Column(modifier = Modifier
                            .fillMaxSize()
                            .fillMaxHeight()
                            .verticalScroll(rememberScrollState(), true),
                            horizontalAlignment = Alignment.CenterHorizontally
                        ){
                            NameRow(name)
                            DropSetSwitch(dropset)
                            TargetMuscleRow(targetmuscles)
                            WeightsRow(dropset,weight1,weight2,weight3)
                            RepsRow(reps)
                            SetsRow(dropset, sets)
                            TimePerSetRow(timeperset,sets)
                            TotalTimeRow(timeperset, sets)
                            ExerciseImage(imagepath)
                            InstructionsRow(instructions)
                        }
                    }
                }

            }
        )
    }
}

//Function to save and update the existing
fun UpdateExistingExercise(name : MutableState<String>,
                    dropset : MutableState<Boolean>,
                    targetmuscles : MutableState<String>,
                    weight1 : MutableState<Double>,
                    weight2 : MutableState<Double>,
                    weight3 : MutableState<Double>,
                    reps : MutableState<Int>,
                    sets : MutableState<Int>,
                    timeperset : MutableState<Int>,
                    instructions : MutableState<String>,
                    imagepath : MutableState<String>, exerciseViewModel : ExerciseViewModel, exercise: Exercise){
    //Set all the existing values with their edit/unedited values
    exercise.name = name.value
    exercise.dropSet = dropset.value
    exercise.targetMuscles = targetmuscles.value
    exercise.weight1 = weight1.value
    //If there is a drop set different values will need to be defined
    if (dropset.value == false){
        exercise.weight2 = 0.0
        exercise.weight3 = 0.0
    }else{
        exercise.weight2 = weight2.value
        exercise.weight3 = weight3.value
    }
    exercise.reps = reps.value
    exercise.sets = sets.value
    exercise.timePerSetInSeconds = timeperset.value
    exercise.instructions = instructions.value
    exercise.imagePath = imagepath.value
    //update the new exercise
    exerciseViewModel.updateExercise(exercise)
}

@Preview
@Composable
private fun EditExerciseFormPreview() {
    val navController = rememberNavController()
    val sessionViewModel: SessionViewModel = viewModel()
    val exerciseViewModel : ExerciseViewModel = viewModel()
    val allExercises by exerciseViewModel.allExercises.observeAsState(listOf())
    val exerciseDefault = allExercises[0]
    Gym_ApplicationTheme(dynamicColor = false) {
        EditExerciseForm(navController, sessionViewModel,exerciseViewModel, exercise = exerciseDefault)
    }
}