package com.example.gym_application.ui.model

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update

@Dao

interface SessionDao {
    @Insert
    fun insertSingleSession(session: Session)

    @Insert
    fun insertMultipleSession(sessionList : List<Session>)

    @Update
    fun updateSession(session: Session)

    @Delete
    fun deleteSession(session: Session)

    @Query("DELETE FROM sessions")
    fun deleteAll()

    @Query("SELECT * FROM sessions")
    fun getAllSession():LiveData<List<Session>>
}